/**
*Главный класс
*@author Kiselev Artem
*/
public class plab1 {
	/**
	*Метод для запуска всей программы
	*@param String array
	*/
	public static void main(String[] args){
		long[] n = new long[18];
		float[] x = new float[13];
		double[][] d = new double[18][13];

		for(int i = 0; i < 18; i++)
			n[i] = i + 2;

		for(int i = 0; i < 13; i++)
			x[i] =(float)(Math.random()*20 - 12);
		
		for(int i = 0; i < 18; i++)
			for(int j = 0; j < 13; j++)
				if(n[i] == 17)
					d[i][j] = 0.5 * Math.asin(1/Math.exp(Math.abs(x[j])));
				else if(n[i] == 2 || n[i] == 3 || n[i] == 5 || n[i] == 7 ||
						 n[i] == 10 || n[i] == 12 || n[i] == 14 || n[i] == 18 || n[i] == 19)
					d[i][j] = Math.log(Math.acos(1/Math.exp(Math.abs(x[j]))));
				else
					d[i][j] = Math.pow( Math.exp(Math.cos(Math.exp(x[j])))*2 ,3);

		for(int i = 0; i < 18; i++){
			for(int j = 0; j < 13; j++)
				System.out.printf("%9.3f", d[i][j]);	
			System.out.println();
		}
	}
}